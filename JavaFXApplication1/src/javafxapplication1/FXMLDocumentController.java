/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Button RestartBtn;
    @FXML
    private Label scoreLabel;
    @FXML
    private Label L0;
    @FXML
    private Label L1;
    @FXML
    private Label L2;
    @FXML
    private Label L3;
    @FXML
    private Label L4;
    @FXML
    private Label L5;
    @FXML
    private Label L6;
    @FXML
    private Label L7;
    @FXML
    private Label L8;
    @FXML
    private Label L9;
    @FXML
    private Label L10;
    @FXML
    private Label L11;
    @FXML
    private Label L12;
    @FXML
    private Label L13;
    @FXML
    private Label L14;
    @FXML
    private Label L15;
    
    int Index[] = new int[16];

    int indexOld1 = -1;
    int score = 0;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
    }

    //@FXML
    @FXML
    private void handleButtonAction(MouseEvent event) {

        Random ran = new Random();
        int tmpIndex;
        int flag = 0;
        tmpIndex = ran.nextInt(16);
        Index[0] = tmpIndex;
        for (int i = 0; i < 16; i++) {
            while (true) {
                tmpIndex = ran.nextInt(16);
                for (int j = 0; j < i; j++) {
                    if (Index[j] == tmpIndex) {
                        flag = 1;
                        break;
                    } else {
                        flag = 0;
                    }
                }
                if (flag == 0) {
                    break;
                }
            }
            Index[i] = tmpIndex;
        }
            
    }

    @FXML
    private void openLabel(MouseEvent event) {
        
    }
    
}
